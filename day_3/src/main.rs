use itertools::Itertools;
use std::collections::HashSet;
use std::fs;

fn main() {
    let input = load_input("input");
    part_one(input.as_str());
    part_two(input.as_str());
}

fn load_input(filename: &str) -> String {
    fs::read_to_string(filename).expect("could not open file")
}

fn part_one(input: &str) {
    let priority: isize = input
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| {
            let halves = l.split_at(l.len() / 2);
            let first = HashSet::<char>::from_iter(halves.0.chars());
            let second = HashSet::<char>::from_iter(halves.1.chars());
            let mut intersection = first.intersection(&second);
            let in_both = intersection.next().expect("iterator empty");
            if intersection.next().is_some() {
                panic!("iterator too long")
            }
            get_priority(*in_both)
        })
        .sum();

    println!("Part 1: {}", priority);
}

fn part_two(input: &str) {
    let priority: isize = input
        .split('\n')
        .chunks(3)
        .into_iter()
        .filter_map(|it| {
            it.fold(Some(make_full_set()), |set, l| {
                if l.is_empty() {
                    return None;
                }
                let set = set.unwrap();
                let chars = HashSet::from_iter(l.chars());
                let intersection = chars.intersection(&set);
                Some(HashSet::from_iter(intersection.copied()))
            })
        })
        .map(|set| {
            let mut it = set.iter();
            let ch = it.next().expect("it should not be empty");
            if it.next().is_some() {
                panic!("iterator too long");
            }
            get_priority(*ch)
        })
        .sum();

    println!("Part 2: {}", priority);
}

fn make_full_set() -> HashSet<char> {
    let mut s = HashSet::new();

    for ch in 'a'..='z' {
        s.insert(ch);
    }

    for ch in 'A'..='Z' {
        s.insert(ch);
    }

    s
}

fn get_priority(ch: char) -> isize {
    if ch.is_ascii_uppercase() {
        (ch as isize) - ('A' as isize) + 27
    } else if ch.is_ascii_lowercase() {
        (ch as isize) - ('a' as isize) + 1
    } else {
        panic!("{} is not ascii", ch);
    }
}

#[test]
fn test_get_priority() {
    assert_eq!(get_priority('a'), 1);
    assert_eq!(get_priority('z'), 26);
    assert_eq!(get_priority('A'), 27);
    assert_eq!(get_priority('Z'), 52);
}
