#! /bin/python

sacks = []
with open("input", "r") as f:
    for line in f:
        line = line.strip()
        sacks.append((line[:len(line)//2], line[len(line)//2:]))

priorities = "0abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
priority = 0

for sack in sacks:
    intersection = set(sack[0]).intersection(set(sack[1]))
    assert(len(intersection) == 1)
    ch = list(intersection)[0]
    priority += priorities.index(ch)

print(priority)

priority = 0
for i in range(0, len(sacks), 3):
    sack1 = set(sacks[i][0]).union(set(sacks[i][1]))
    sack2 = set(sacks[i+1][0]).union(set(sacks[i+1][1]))
    sack3 = set(sacks[i+2][0]).union(set(sacks[i+2][1]))

    ch = list(sack1.intersection(sack2).intersection(sack3))[0]
    priority += priorities.index(ch)
print(priority)
