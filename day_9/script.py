#! /bin/python

moves = []
with open("input", "r") as f:
    for line in f:
        moves = [*moves, *(line[0] for i in range(int(line[2:])))]

ends = [(0, 0) for _ in range(10)]
first_visited = set()
last_visited = set()

for move in moves:
    if move == 'U':
        ends[0] = (ends[0][0], ends[0][1]+1)
    elif move == 'D':
        ends[0] = (ends[0][0], ends[0][1]-1)
    elif move == 'L':
        ends[0] = (ends[0][0]-1, ends[0][1])
    elif move == 'R':
        ends[0] = (ends[0][0]+1, ends[0][1])
    else:
        raise ValueError(move)

    for i in range(1, 10):
        x_diff = (ends[i-1][0] - ends[i][0])
        y_diff = (ends[i-1][1] - ends[i][1])
        if abs(x_diff) == 2 and abs(y_diff) == 1:
            y_diff *= 2
        elif abs(x_diff) == 1 and abs(y_diff) == 2:
            x_diff *= 2
        x_diff = int(x_diff / 2)
        y_diff = int(y_diff / 2)
        ends[i]= (ends[i][0] + x_diff, ends[i][1] + y_diff)
        if i == 1:
            first_visited.add(ends[i])
        if i == 9:
            last_visited.add(ends[i])

    # print(f"ends[0]: {ends[0]}")
    # print(f"tail: {tail}")
    # board = [['.' for _ in range(6)] for _ in range(6)]
    # for spot in visited:
        # board[(-1-spot[1])%6][spot[0]] = '*'
    # board[(-1-ends[0][1])%6][ends[0][0]] = 'H'
    # board[(-1-tail[1])%6][tail[0]] = 'T'
    # print("\n".join([" ".join(row) for row in board]))
    # print("")

# board = [['.' for _ in range(6)] for _ in range(6)]
# for spot in visited:
    # board[(-1-spot[1])%6][spot[0]] = '*'
# print("\n".join([" ".join(row) for row in board]))
print(len(first_visited))
print(len(last_visited))
