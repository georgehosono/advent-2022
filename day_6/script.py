#! /bin/python

def find_start(s, num):
    codeword = []
    for i in range(len(s)):
        codeword.append(s[i])
        if len(codeword) > num:
            codeword.pop(0)
            if len(set(codeword)) == num:
                return i+1

with open("input", "r") as f:
    text = f.read()

print(find_start(text, 4))
print(find_start(text, 14))
