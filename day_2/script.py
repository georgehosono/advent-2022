#! /bin/python

matches = []
with open("input", "r") as f:
    for line in f:
        matches.append(tuple(line.split()))

def score(game):
    score = 0
    if game[1] == 'X':
        score += 1
        my_move = 0
    elif game[1] == 'Y':
        score += 2
        my_move = 1
    elif game[1] == 'Z':
        score += 3
        my_move = 2

    if game[0] == 'A':
        their_move = 0
    elif game[0] == 'B':
        their_move = 1
    elif game[0] == 'C':
        their_move = 2

    outcome = (my_move - their_move) % 3
    if outcome == 0:
        score += 3
    elif outcome == 1:
        score += 6
    elif outcome == 2:
        score += 0

    return score

def alt_score(game):
    score = 0

    if game[0] == 'A':
        their_move = 0
    elif game[0] == 'B':
        their_move = 1
    elif game[0] == 'C':
        their_move = 2

    if game[1] == 'X':
        score += 0
        my_move = (their_move + 2) % 3
    elif game[1] == 'Y':
        score += 3
        my_move = their_move
    elif game[1] == 'Z':
        score += 6
        my_move = (their_move + 1) % 3

    score += my_move + 1

    return score

print(sum(alt_score(game) for game in matches))
