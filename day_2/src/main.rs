#![deny(clippy::all)]

use std::fs;

fn main() {
    let s = fs::read_to_string("input").unwrap();
    part_one(s.as_str());
    part_two(s.as_str());
}

fn part_one(input: &str) {
    let score: isize = input
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.split(' ')
                .map(|v| {
                    v.chars()
                        .next()
                        .expect("empty string")
                        .try_into()
                        .expect("could not construct")
                })
                .collect::<Vec<RPS>>()
                .try_into()
                .expect("not exactly 2 elements")
        })
        .map(|game| score_game(&game))
        .sum();

    println!("Part 1 score: {}", score);
}

fn part_two(input: &str) {
    let score: isize = input
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| {
            l.split(' ')
                .map(|v| v.chars().next().expect("empty string"))
                .collect::<Vec<char>>()
        })
        .map(|v| {
            assert!(v.len() == 2);
            let theirs: RPS = v[0].try_into().expect("invalid char");
            let ours = match v[1] {
                'X' => match theirs {
                    RPS::Rock => RPS::Scissors,
                    RPS::Paper => RPS::Rock,
                    RPS::Scissors => RPS::Paper,
                },
                'Y' => theirs.clone(),
                'Z' => match theirs {
                    RPS::Rock => RPS::Paper,
                    RPS::Paper => RPS::Scissors,
                    RPS::Scissors => RPS::Rock,
                },
                _ => panic!("invalid char"),
            };
            score_game(&[theirs, ours])
        })
        .sum();

    println!("Part 2 score: {}", score);
}

fn score_game(game: &[RPS; 2]) -> isize {
    let mut score = 0;
    score += game[1].get_num() + 1;
    score += match (3 + game[0].get_num() - game[1].get_num()) % 3 {
        // draw
        0 => 3,

        // loss
        1 => 0,

        // win
        2 => 6,
        x => unreachable!("got {} after mod 3", x),
    };

    score
}

#[derive(Debug, Clone)]
enum RPS {
    Rock,
    Paper,
    Scissors,
}

impl std::convert::TryFrom<char> for RPS {
    type Error = &'static str;
    fn try_from(ch: char) -> Result<RPS, Self::Error> {
        match ch {
            'A' => Ok(RPS::Rock),
            'B' => Ok(RPS::Paper),
            'C' => Ok(RPS::Scissors),
            'X' => Ok(RPS::Rock),
            'Y' => Ok(RPS::Paper),
            'Z' => Ok(RPS::Scissors),
            _ => Err("invalid character"),
        }
    }
}

impl RPS {
    fn get_num(&self) -> isize {
        match self {
            Self::Rock => 0,
            Self::Paper => 1,
            Self::Scissors => 2,
        }
    }
}
