use std::fs;
use std::collections::BTreeMap;

fn main() {
    let input = fs::read_to_string("input").expect("could not read file");
    part_one(input.as_str());
    part_two(input.as_str());
}

fn part_one(s: &str) {
    let mut hill = parse_input(s);
    let path_len = hill.find_path().unwrap();
    println!("part one: {}", path_len);
}

fn part_two(s: &str) {
    //let mut hill = parse_input(s);
    //let mut low_points: BTreeMap<(isize, isize), Option<isize>> = hill.vertecies.iter()
        //.filter(|(_, node)| node.height == 'a' as isize)
        //.map(|(coords, _)| (*coords, None))
        //.collect();

    //dbg!(low_points.len());

    //for (coord, dist) in low_points.iter_mut() {
        //dbg!(coord);
        //hill.reset();
        //hill.vertecies.get_mut(coord).unwrap().node_type = HillNodeType::Start;
        //hill.vertecies.get_mut(coord).unwrap().distance = Some(0);

        //*dist = hill.find_path();
    //}

    //let min = low_points.values()
        //.filter_map(|n| *n)
        //.min()
        //.unwrap();
    
    let mut hill = parse_input(s);
    hill.reset();
    for (_, node) in hill.vertecies.iter_mut() {
        if let HillNodeType::End = node.node_type {
            node.distance = Some(0);
            break;
        }
    }

    let path_len = hill.find_reverse_path().unwrap();

    println!("part two: {}", path_len);
}

fn parse_input(s: &str) -> Hill {
    let mut hill = Hill::new();

    let it = s
        .split('\n')
        .filter(|l| !l.is_empty())
        .enumerate()
        .flat_map(|(n0, l)| l.chars()
            .enumerate()
            .map(move |(n1, ch)| ((n0, n1), ch)));

    for ((n0, n1), ch) in it {
        hill.add_node((n0 as isize, n1 as isize), ch);
    }

    hill.calculate_edges();
    hill
}

#[derive(Copy, Clone, Debug)]
enum HillNodeType {
    Start,
    End,
    Regular,
}

#[derive(Copy, Clone, Debug)]
struct HillNode {
    height: isize,
    node_type: HillNodeType,
    distance: Option<isize>,
}

impl HillNode {
    fn new(ch: char) -> Self {
        let (height, node_type, distance) = match ch {
            'S' => ('a' as isize, HillNodeType::Start, Some(0)),
            'E' => ('z' as isize, HillNodeType::End, None),
            _ => (ch as isize, HillNodeType::Regular, None),
        };
        HillNode {
            height,
            node_type,
            distance,
        }
    }
}

#[derive(Debug)]
struct Hill {
    vertecies: BTreeMap<(isize, isize), HillNode>,
    edges: Vec<((isize, isize), (isize, isize))>,
}

impl Hill {
    fn new() -> Self {
        Hill {
            vertecies: BTreeMap::new(),
            edges: Vec::new(),
        }
    }

    fn add_node(&mut self, coordinates: (isize, isize), ch: char) {
        let node = HillNode::new(ch);
        self.vertecies.insert(coordinates, node);
    }

    fn calculate_edges(&mut self) {
        for (coords, node) in self.vertecies.iter() {
            for offset in [(1,0), (-1, 0), (0, 1), (0, -1)] {
                let new_coords = (coords.0+offset.0, coords.1+offset.1);
                if let Some(neighbor) = self.vertecies.get(&new_coords) {
                    if neighbor.height-1 <= node.height {
                        self.edges.push((*coords, new_coords));
                    }
                }
            }
        }
    }

    fn get_outgoing_neighbors(&self, coords: &(isize, isize)) -> Vec<(isize, isize)> {
       self.vertecies.get(coords).expect("call with bad coordinates"); 
       let mut v = Vec::new();

       for edge in self.edges.iter() {
           if edge.0 == *coords {
               v.push(edge.1);
           }
       }
       v
    }

    fn get_incoming_neighbors(&self, coords: &(isize, isize)) -> Vec<(isize, isize)> {
       self.vertecies.get(coords).expect("call with bad coordinates"); 
       let mut v = Vec::new();

       for edge in self.edges.iter() {
           if edge.1 == *coords {
               v.push(edge.0);
           }
       }
       v
    }
    fn find_reverse_path(&mut self) -> Option<isize> {
        for _ in 0..self.vertecies.len() {
            let mut new_vertecies = BTreeMap::new();
            for (coords, node) in self.vertecies.iter() {
                let mut node: HillNode = *node;
                for neighbor in self.get_outgoing_neighbors(coords).iter() {
                    let neighbor = self.vertecies.get(neighbor).unwrap();
                    if let Some(neighbor_dist) = neighbor.distance {
                        //dbg!(neighbor);
                        match node.distance {
                            Some(dist) => {
                                if dist > neighbor_dist {
                                    node.distance = Some(neighbor_dist + 1);
                                }
                            },
                            None => node.distance = Some(neighbor_dist + 1),
                        }
                    }
                }

                if let Some(d) = node.distance {
                    if node.height == 'a' as isize {
                        return Some(d);
                    }
                }

                new_vertecies.insert(*coords, node);
            }
            self.vertecies = new_vertecies;
        }

        None
    }

    fn find_path(&mut self) -> Option<isize> {
        for _ in 0..self.vertecies.len() {
            let mut new_vertecies = BTreeMap::new();
            for (coords, node) in self.vertecies.iter() {
                let mut node: HillNode = *node;
                for neighbor in self.get_incoming_neighbors(coords).iter() {
                    let neighbor = self.vertecies.get(neighbor).unwrap();
                    if let Some(neighbor_dist) = neighbor.distance {
                        //dbg!(neighbor);
                        match node.distance {
                            Some(dist) => {
                                if dist > neighbor_dist {
                                    node.distance = Some(neighbor_dist + 1);
                                }
                            },
                            None => node.distance = Some(neighbor_dist + 1),
                        }
                    }
                }

                if let Some(d) = node.distance {
                    if let HillNodeType::End = node.node_type {
                        return Some(d);
                    }
                }

                new_vertecies.insert(*coords, node);
            }
            self.vertecies = new_vertecies;
        }

        None
    }

    fn reset (&mut self) {
        for (_, node) in self.vertecies.iter_mut() {
            node.distance = None;
            if let HillNodeType::Start = node.node_type {
                node.node_type = HillNodeType::Regular;
            }
        }
    }

    #[allow(dead_code)]
    fn bfs(self, mut paths: Vec<Vec<(isize, isize)>>) -> Vec<(isize, isize)> {
        let mut new_paths = Vec::new();
        dbg!(paths.len());
        for path in paths.drain(..) {
            let neighbors = self.get_outgoing_neighbors(&path[path.len()-1]);
            for neighbor in neighbors.iter() {
                if !path.contains(neighbor) {
                    let mut new_path = path.clone();
                    new_path.push(*neighbor);
                    if let HillNodeType::End = self.vertecies[neighbor].node_type {
                        return new_path
                    };
                    new_paths.push(new_path); 
                }
            }
        }
        self.bfs(new_paths)
    }
}
