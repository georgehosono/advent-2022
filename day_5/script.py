#! /bin/python

import re

with open("input", "r") as f:
    text = f.read()

halves = text.split("\n\n")

crate_text = halves[0]
crates = {}
for i in range(1, 36, 4):
    crates[i//4+1] = [crate_text[j] for j in range(i, len(crate_text), 36) if crate_text[j].isalpha()]

def print_crates():
    print("crates:")
    for i in sorted(crates.keys()):
        print(f"{i}: {crates[i]}")
    print("")

move_text = halves[1].strip()
moves = []
for move in move_text.split("\n"):
    r = re.match(r"move (\d\d?) from (\d) to (\d)", move)
    moves.append( (int(r[1]), int(r[2]), int(r[3])) )

# Part 1
# print_crates()
# for move in moves:
    # print(f"Move {move[0]} from {move[1]} to {move[2]}")
    # for i in range(move[0]):
        # crate = crates[move[1]].pop(0)
        # crates[move[2]].insert(0, crate)
    # print_crates()

# Part 2
print_crates()
for move in moves:
    print(f"Move {move[0]} from {move[1]} to {move[2]}")
    crates[move[2]] = crates[move[1]][:move[0]] + crates[move[2]]
    crates[move[1]] = crates[move[1]][move[0]:]
    print_crates()
