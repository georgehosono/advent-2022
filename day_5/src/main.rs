use std::fs;
use regex::Regex;

fn main() {
    let s = fs::read_to_string("input").expect("could not open");
    part_one(s.as_str());
    part_two(s.as_str());
}

fn parse_stacks(s: &str) -> Vec<Vec<char>> {
    let stacks = s.split("\n\n").next().expect("could not parse");
    let mut lines = stacks.split('\n').peekable();

    let mut stacks: Vec<Vec<char>> = Vec::new();
    for _ in 0..(lines.peek().expect("empty").len()+1)/4 {
        stacks.push(Vec::new());
    }

    for line in lines {
        for (i, ch) in line.chars().skip(1).step_by(4).enumerate() {
            if ch.is_alphabetic() {
                stacks[i].push(ch);
            }
        }
    }

    stacks
}

struct Move {
    count: u8,
    from: u8,
    to: u8,
}

fn parse_moves(s: &str) {
    let re = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();
    &s.split("\n\n")
        .nth(1)
        .expect("could not parse")
        .split('\n')
        .map(move |l| {
            let matches = &re.captures(l).expect("can't match move");
            Move{
                count: matches.get(1).unwrap().as_str().parse().unwrap(),
                from: matches.get(2).unwrap().as_str().parse().unwrap(),
                to: matches.get(3).unwrap().as_str().parse().unwrap(),
            }
        })
    .into_iter()
}

fn part_one(s: &str) {
    let stacks = parse_stacks(s);
}

fn part_two(s: &str) {
}
