use regex::Regex;
use std::fs;

fn main() {
    let s = fs::read_to_string("input").expect("could not open");
    part_one(s.as_str());
}

fn part_one(s: &str) {
    let re = Regex::new(r"(\d+)-(\d+),(\d+)-(\d+)").expect("bad regex");
    let contained: isize = s
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| {
            let caps = re.captures(l).expect("no match found");
            let r1_low: isize = caps
                .get(1)
                .expect("no capture")
                .as_str()
                .parse()
                .expect("could not convert");
            let r1_high: isize = caps
                .get(2)
                .expect("no capture")
                .as_str()
                .parse()
                .expect("could not convert");
            let r2_low: isize = caps
                .get(3)
                .expect("no capture")
                .as_str()
                .parse()
                .expect("could not convert");
            let r2_high: isize = caps
                .get(4)
                .expect("no capture")
                .as_str()
                .parse()
                .expect("could not convert");
            if (r1_low <= r2_low && r1_high >= r2_high) || (r2_low <= r1_low && r2_high >= r1_high)
            {
                1
            } else {
                0
            }
        })
        .sum();

    println!("Part 1: {}", contained);
}
