#! /bin/python

with open("input", "r") as f:
    count = 0
    for line in f:
        elves = line.split(",")
        elves = [elf.split("-") for elf in elves]
        elf1 = set(range(int(elves[0][0]), int(elves[0][1])+1))
        elf2 = set(range(int(elves[1][0]), int(elves[1][1])+1))

        if elf1.issubset(elf2) or elf2.issubset(elf1):
            count += 1

print(count)

count = 0
with open("input", "r") as f:
    count = 0
    for line in f:
        elves = line.split(",")
        elves = [elf.split("-") for elf in elves]
        elf1 = set(range(int(elves[0][0]), int(elves[0][1])+1))
        elf2 = set(range(int(elves[1][0]), int(elves[1][1])+1))

        if not elf1.isdisjoint(elf2):
            count += 1

print(count)
