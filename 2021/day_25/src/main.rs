use std::fs;
use std::fmt;
use itertools::Itertools;

fn main() {
    let mut sea = Sea::from_file("cucumbers");

    println!("Initial state");
    println!("{}", &sea);

    let mut steps = 0;
    while sea.step() {
        steps += 1;
        println!("\nAfter {} steps", steps);
        println!("{}", sea);
    }
}

#[derive(Copy, Clone, Debug)]
enum Spot {
    Empty,
    East,
    South,
}

impl fmt::Display for Spot {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let ch = match self {
            Spot::Empty => '.',
            Spot::East => '>',
            Spot::South => 'v',
        };
        write!(f, "{}", ch)
    }
}

impl From<char> for Spot {
    fn from(ch: char) -> Self {
        match ch {
            '.' => Spot::Empty,
            '>' => Spot::East,
            'v' => Spot::South,
            _ => panic!("could not match char"),
        }
    }
}

struct Sea {
    floor: Vec<Vec<(Spot, bool)>>,
}

impl Sea {
    fn from_file(fname: &str) -> Self {
        let text = fs::read_to_string(fname).expect("could not read");
        Sea {
            floor: Vec::from_iter(
                       text.split('\n')
                       .filter(|l| !l.is_empty())
                       .map(|l| Vec::from_iter(
                               l.chars()
                               .map(|ch| (Spot::from(ch), false)))
                           )
                       )
        }
    }

    fn step(&mut self) -> bool {
        let mut changed = false;

        for i in 0..self.floor.len() {
            for j in 0..self.floor[i].len() {
                let east_len = self.floor[i].len();
                if let (Spot::East, false) = self.floor[i][j] {
                    if let (Spot::Empty, false) = self.floor[i][(j+1) % east_len] {
                        changed = true;
                        self.floor[i][j] = (Spot::Empty, true);
                        self.floor[i][(j+1) % east_len] = (Spot::East, true);
                    }
                }
            }
        }

        for row in self.floor.iter_mut() {
            for spot in row.iter_mut() {
                spot.1 = false;
            }
        }

        for i in 0..self.floor.len() {
            let south_len = self.floor.len();
            for j in 0..self.floor[i].len() {
                if let (Spot::South, false) = self.floor[i][j] {
                    if let (Spot::Empty, false) = self.floor[(i+1) % south_len][j] {
                        changed = true;
                        self.floor[i][j] = (Spot::Empty, true);
                        self.floor[(i+1)%south_len][j] = (Spot::South, true);
                    }
                }
            }
        }

        for row in self.floor.iter_mut() {
            for spot in row.iter_mut() {
                spot.1 = false;
            }
        }

        changed
    }
}

#[allow(unstable_name_collisions)]
impl fmt::Display for Sea {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = self.floor
            .iter()
            .map(|line| line.iter()
                .map(|spot| format!("{}", spot.0))
                .collect::<String>())
            .intersperse(String::from("\n"))
            .collect::<String>();
        write!(f, "{}", s)
    }
}
