use std::fs;
use regex::Regex;
use petgraph::Graph;
use petgraph::Directed;
use petgraph::graph::NodeIndex;
use petgraph::visit::EdgeRef;

fn main() {
    let s = fs::read_to_string("test").expect("could not open");
    let mut v: Volcano = s.as_str().into();
    v.traverse();
    dbg!(v);
}

/*
 *#[derive(Debug)]
 *struct Valve {
 *    flow: isize,
 *    name: String,
 *    open: bool,
 *}
 *
 *impl Valve {
 *    fn new(flow: isize, name: String) -> Self {
 *        Valve {
 *            flow,
 *            name,
 *            open: false,
 *        }
 *    }
 *}
 */

#[derive(Debug)]
struct Edge {
    flow: isize,
    blocked: bool,
}

#[derive(Debug)]
struct Volcano {
    graph: Graph<String, Edge>,
    start: NodeIndex,
}

impl Volcano {
    pub fn traverse(&mut self) -> isize {
        self.traverse_internal(self.start, 30, 0)
    }

    fn traverse_internal(&mut self, curr_node: NodeIndex, min_left: isize, pressure_released: isize) -> isize {
        // base case
        if min_left == 0 {
            return pressure_released;
        }

        for e in self.graph.edges(curr_node).filter(|e| !e.weight().blocked) {
            if e.weight().flow > 0 {
                self.graph.edge_weight_mut(
                    self.graph.edge_indices().nth(e.id())).unwrap().blocked = true;
                //self.graph[e.id()].blocked = true;
            }
            //self.traverse_internal(e.target(), min_left-1, pressure_released + e.weight().flow * (min_left - 1));
        }

        /*
         *self.graph.edges(curr_node)
         *    .filter(|e| !e.weight().blocked)
         *    .map(|e| {
         *        if e.weight().flow > 0 {
         *            self.graph[e.id()].blocked = true;
         *        }
         *        let ret = self.traverse_internal(e.target(), min_left - 1, pressure_released + e.weight().flow * (min_left - 1));
         *        self.graph[e.id()].blocked = false;
         *        ret
         *    })
         *    .max();
         */

        return 0;
    }
}

fn get_or_insert<V: Eq, E>(graph: &mut Graph<V, E>, value: V) -> NodeIndex {
    graph.node_indices().find(|i| graph[*i] == value)
        .unwrap_or_else(|| graph.add_node(value))
}

impl<'a> From<&'a str> for Volcano {
    fn from(s: &'a str) -> Self {
        let re = Regex::new(r"Valve ([A-Z]+) has flow rate=(\d+); tunnels? leads? to valves? (.+)").unwrap();
        
        let mut graph = Graph::<String, Edge, Directed>::default();

        s.split('\n')
            .filter(|l| !l.is_empty())
            .for_each(|l| {
                let captures = re.captures(l).unwrap();
                let name: &str = captures.get(1).unwrap().as_str();
                let rate: isize = captures.get(2).unwrap().as_str().parse().unwrap();

                let closed_idx = get_or_insert(&mut graph, name.into());
                let mut open_name: String = name.to_string();
                open_name.push_str("_opened");
                let open_idx = get_or_insert(&mut graph, open_name.into());

                captures.get(3).unwrap().as_str().split(", ")
                    .for_each(|conn| {
                        // Get or insert node index
                        get_or_insert(&mut graph, conn.into());
                    });

                graph.add_edge(closed_idx, open_idx, Edge{flow: rate, blocked: false});
            });
        let start = graph.node_indices().find(|i| graph[*i] == "AA").expect("No node AA");

        Volcano{
            graph,
            start,
        }
    }
}
