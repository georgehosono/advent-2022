#! /bin/python

import re
import itertools

class Valve:
    prog = re.compile(r"Valve (\w\w) has flow rate=(\d+); tunnels? leads? to valves? (.*)")

    def __init__(self, id: str, rate: int, neighbors: list[str]):
        self.id = id
        self.rate = rate
        self.neighbors = neighbors
        self.distances = {}
        self.open = False

        self.dijkstra = None

    def from_line(line):
        results = Valve.prog.match(line)
        id = results.group(1)
        rate = int(results.group(2))
        neighbors = results.group(3).split(", ")
        
        return Valve(id, rate, neighbors)

    def __str__(self):
        return f"Valve {self.id}. Rate {self.rate}. Distances {self.distances}. Open? {self.open}. Neighbors: {self.neighbors}"

class Volcano:
    def __init__(self):
        self.valves = {}

    def add_valve(self, valve):
        self.valves[valve.id] = valve

    def compute_all_distances(self):
        for id in self.valves.keys():
            self.compute_distances(id)

        for id, valve in self.valves.copy().items():
            if valve.rate == 0 and id != "AA":
                self.valves.pop(id)

    def compute_distances(self, id):
        curr_valve = self.valves[id]
        unvisited = set(self.valves.keys())

        # set all distances to infinite
        for v in self.valves.values():
            v.dijkstra = float("inf")

        # set own distance to 0
        curr_valve.dijkstra = 0

        # loop until all nodes are visited
        while len(unvisited) > 0:
            # choose the node with the smallest distance
            curr_valve = min(unvisited, key=lambda id: self.valves[id].dijkstra)
            curr_valve = self.valves[curr_valve]

            # check if we've visited here before
            for neighbor in curr_valve.neighbors:
                if neighbor not in unvisited:
                    continue

                # if we have a closer route, update the path
                if curr_valve.dijkstra + 1 < self.valves[neighbor].dijkstra:
                    self.valves[neighbor].dijkstra = curr_valve.dijkstra + 1
            unvisited.remove(curr_valve.id)

        for valve in self.valves.values():
            self.valves[id].distances[valve.id] = valve.dijkstra

    def path_helper(self, start, time, current):
        paths = []
        for id, v in self.valves[start].distances.items():
            if id not in self.valves:
                continue
            time_left = time - self.valves[start].distances[id] - 1
            if time_left < 0 or id in current:
                continue
            new_paths = self.path_helper(id, time_left, current + [id])
            for path in new_paths:
                paths.append(path)
        if len(paths) == 0:
            return [current]
        return paths

    def paths(self, time):
        return sorted(self.path_helper("AA", time, ["AA"]))

    def pair_path_helper(self, path1, path2):
        pairs = []

        # Extend path1
        for id, v in self.valves[path1[-1]].distances.items():
            if id not in self.valves:
                continue
            if id not in path1 and id not in path2 and self.time_path(path1 + [id]) <= 26:
                new_paths = self.pair_path_helper(path1 + [id], path2)
                for path in new_paths:
                    pairs.append(path)

        # Extend path2
        for id, v in self.valves[path2[-1]].distances.items():
            if id not in self.valves:
                continue
            # Put id in path2
            if id not in path1 and id not in path2  and self.time_path(path2 + [id]) <= 26:
                new_paths = self.pair_path_helper(path1, path2 + [id])
                for path in new_paths:
                    pairs.append(path)

        if len(pairs) == 0:
            return [(path1, path2)]
        return pairs

    # def pair_paths(self):
    #     return sorted(self.pair_path_helper(["AA"], ["AA"]))
           
    def time_path(self, path):
        time = 0
        for i in range(len(path)-1):
            time += self.valves[path[i]].distances[path[i+1]] + 1
        return time

    def weigh_path(self, path, time=30):
        pressure = 0
        for i in range(len(path) - 1):
            time -= self.valves[path[i]].distances[path[i+1]] + 1
            pressure += time * self.valves[path[i+1]].rate
        return pressure
            

    def compute_path(self, curr_valve, time):
        curr_valve = self.valves[curr_valve]

        options = [0]
        for valve in self.valves.values():
            if valve.open:
                continue
            valve.open = True
            time_left = time - curr_valve.distances[valve.id] - 1
            options.append(valve.rate * time_left + self.compute_path(valve.id, time_left)) 
            valve.open = False
        return max(options)

volcano = Volcano()
with open("input", "r") as f:
    for line in f:
        v = Valve.from_line(line)
        volcano.add_valve(v)

volcano.compute_all_distances()

paths = volcano.paths(30)
print(f"There are {len(paths)} paths")

print(max([volcano.weigh_path(path) for path in paths]))    

short_paths = sorted(volcano.paths(26), reverse=True, key=lambda p: volcano.weigh_path(p))

new_short_paths = []
max_len = len(max(short_paths, key=lambda p: len(p)))
for i in range(2, max_len):
    for path in short_paths:
        if len(path) > i:
            new_short_paths.append(path[:i])
short_paths += new_short_paths
short_paths = sorted(short_paths, reverse=True, key=lambda p: volcano.weigh_path(p))
print(f"there are {len(short_paths)} short paths")

pair_paths = itertools.combinations(short_paths[:10000], 2)
valid_pairs = filter(lambda p: set(p[0][1:]).isdisjoint(set(p[1][1:])), pair_paths)

print(max((volcano.weigh_path(p[0], 26) + volcano.weigh_path(p[1], 26) for p in valid_pairs)))
