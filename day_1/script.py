with open("input", "r") as f:
    text = f.read()

# Part 1
# print(max([sum([int(x) for x in elf.split()]) for elf in text.split("\n\n")]))

# Part 2
print(sum(sorted([sum([int(x) for x in elf.split()]) for elf in text.split("\n\n")])[-3:]))
