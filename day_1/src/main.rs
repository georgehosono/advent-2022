#![deny(clippy::all)]

use itertools::Itertools;
use std::fs;

fn main() {
    let text = fs::read_to_string("input").expect("could not read file");

    // Part 1
    let answer: u32 = text
        .split("\n\n")
        .map(|e| e.split('\n').map(|l| l.parse::<u32>().unwrap_or(0)).sum())
        .max().unwrap();
    println!("{}", answer);

    // Part 2
    let answer: u32 = text
        .split("\n\n")
        .map(|e| {
            e.split('\n')
                .map(|l| l.parse::<u32>().unwrap_or(0))
                .sum::<u32>()
        })
        .sorted_unstable_by(|a, b| Ord::cmp(b, a))
        .take(3)
        .sum::<u32>();
    println!("{}", answer);
}
