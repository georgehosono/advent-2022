#! /bin/python

x = 1
cycles = 0
strengths = 0
with open("input", "r") as f:
    for line in f:
        tokens = line.split()
        cycles += 1
        if (cycles+20) % 40 == 0:
            strengths += x*cycles
        if tokens[0] == "addx":
            cycles += 1
            if (cycles+20) % 40 == 0:
                strengths += x*cycles
            x += int(tokens[1])

print(strengths)

def print_char(cycle, x):
    spot = (cycle-1) % 40
    if spot >= x-1 and spot <= x+1:
        print("#", end="")
    else:
        print(".", end="")
    if cycles % 40 == 0:
        print("")

x = 1
cycles = 0
with open("input", "r") as f:
    for line in f:
        tokens = line.split()
        cycles += 1
        print_char(cycles, x)
        if tokens[0] == "addx":
            cycles += 1
            print_char(cycles, x)
            x += int(tokens[1])
