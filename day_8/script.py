#! /bin/python

forest = []
with open("input", "r") as f:
    for line in f:
        forest.append([int(tree) for tree in line.strip()])

visibility = [[False for tree in row] for row in forest]

for i in range(len(forest)):
    max_height = -1
    for j in range(len(forest[i])):
        if forest[i][j] > max_height:
            visibility[i][j] = True
            max_height = forest[i][j]

for i in range(len(forest)):
    max_height = -1
    for j in range(len(forest[i])-1, -1, -1):
        if forest[i][j] > max_height:
            visibility[i][j] = True
            max_height = forest[i][j]

for j in range(len(forest[0])):
    max_height = -1
    for i in range(len(forest)):
        if forest[i][j] > max_height:
            visibility[i][j] = True
            max_height = forest[i][j]

for j in range(len(forest[0])):
    max_height = -1
    for i in range(len(forest)-1, -1, -1):
        if forest[i][j] > max_height:
            visibility[i][j] = True
            max_height = forest[i][j]

count = 0
for row in visibility:
    for tree in row:
        if tree:
            count += 1

print(count)

scenic_scores = [[None for tree in row] for row in forest]

for i in range(len(forest)):
    for j in range(len(forest[i])):
        height = forest[i][j]
        up = 0
        down = 0
        left = 0
        right = 0

        for up_idx in range(i-1, -1, -1):
            up += 1
            if forest[up_idx][j] >= height:
                break

        for down_idx in range(i+1, len(forest)):
            down += 1
            if forest[down_idx][j] >= height:
                break

        for left_idx in range(j-1, -1, -1):
            left += 1
            if forest[i][left_idx] >= height:
                break

        for right_idx in range(j+1, len(forest[i])):
            right += 1
            if forest[i][right_idx] >= height:
                break

        scenic_scores[i][j] = up * down * left * right

m = 0
for row in scenic_scores:
    m = max(m, max(row))
print(m)
