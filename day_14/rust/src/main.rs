use std::{fs, str, cmp, fmt};
use fxhash::FxHashMap;

fn main() {
    let s = fs::read_to_string("../input").expect("could not open file");
    part_one(s.as_str());
    part_two(s.as_str());
}

fn part_one(s: &str) {
    let mut waterfall: Waterfall = s.into();
    let grains = waterfall.drop_sand_part_one();
    println!("{}", waterfall);
    println!("part one: {}", grains);
}

fn part_two(s: &str) {
    let mut waterfall: Waterfall = s.into();
    waterfall.prep_for_part_two();
    let grains = waterfall.drop_sand_part_two();
    println!("{}", waterfall);
    println!("part two: {}", grains);
}

#[derive(Debug)]
enum Spot {
    Air,
    Rock,
    Sand,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
struct Coordinate {
    x: isize,
    y: isize,
}

impl Coordinate {
    fn new(x: isize, y: isize) -> Self {
        Coordinate {
            x,
            y,
        }
    }
}

impl From<&str> for Coordinate {
    fn from(s: &str) -> Self {
        let mut it = s.split(',');
        let x = it.next().unwrap();
        let y = it.next().unwrap();
        assert!(it.next().is_none());

        let x: isize = x.parse().unwrap();
        let y: isize = y.parse().unwrap();
        Coordinate {
            x,
            y,
        }
    }
}

fn get_path(start: &Coordinate, end: &Coordinate) -> Vec<Coordinate>{
    if start.x == end.x {
        (cmp::min(start.y, end.y)..=cmp::max(start.y, end.y))
            .map(|y| Coordinate::new(start.x, y))
            .collect()
    } else if start.y == end.y {
        (cmp::min(start.x, end.x)..=cmp::max(start.x, end.x))
            .map(|x| Coordinate::new(x, start.y))
            .collect()
    } else {
        panic!("could not make path")
    }
}

#[derive(Debug)]
struct Waterfall {
    spots: FxHashMap<Coordinate, Spot>,
    top_left: Coordinate,
    bottom_right: Coordinate,
}

impl From<&str> for Waterfall {
    fn from(s: &str) -> Self {
        let mut it = s.split('\n')
            .filter(|l| !l.is_empty())
            .map(|l| l.split(" -> ")
                .filter(|c| !c.is_empty())
                .map(Coordinate::from)
                .peekable())
            .peekable();

        let mut spots = FxHashMap::default();
        let mut top_left: Coordinate = *it.peek_mut().unwrap().peek().unwrap();
        let mut bottom_right: Coordinate = top_left;

        for line in it {
            let mut previous: Option<Coordinate> = None;
            for coord in line {
                if coord.x < top_left.x {
                    top_left.x = coord.x;
                }
                if coord.y < top_left.y {
                    top_left.y = coord.y;
                }
                if coord.x > bottom_right.x {
                    bottom_right.x = coord.x;
                }
                if coord.y > bottom_right.y {
                    bottom_right.y = coord.y;
                }

                if let Some(prev) = previous {
                    let path = get_path(&prev, &coord);
                    for spot in path {
                        spots.insert(spot, Spot::Rock);
                    }
                } else {
                    spots.insert(coord, Spot::Rock);
                }
                previous = Some(coord);
            }
        }

        Waterfall {
            spots,
            top_left,
            bottom_right,
        }
    }
}

impl fmt::Display for Waterfall {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        for y in 0..=self.bottom_right.y {
            for x in self.top_left.x..=self.bottom_right.x {
                let coord = Coordinate::new(x, y);
                let spot = self.get(&coord).unwrap_or(&Spot::Air);
                let ch = match spot {
                    Spot::Air => '.',
                    Spot::Rock => '#',
                    Spot::Sand => 'o',
                };
                write!(f, "{}", ch)?
            }
            writeln!(f)?
        }
        Ok(())
    }
}

impl Waterfall {
    fn prep_for_part_two(&mut self) {
        self.bottom_right.y += 2;
    }

    fn get(&self, coord: &Coordinate) -> Option<&Spot> {
        if coord.y >= self.bottom_right.y {
            Some(&Spot::Rock)
        } else {
            self.spots.get(coord)
        }
    }

    fn insert(&mut self, coord: Coordinate, value: Spot) -> Option<Spot> {
        if coord.x < self.top_left.x {
            self.top_left.x = coord.x;
        }
        if coord.y < self.top_left.y {
            self.top_left.y = coord.y;
        }
        if coord.x > self.bottom_right.x {
            self.bottom_right.x = coord.x;
        }
        if coord.y > self.bottom_right.y {
            self.bottom_right.y = coord.y;
        }
        self.spots.insert(coord, value)
    }

    fn drop_sand_part_one(&mut self) -> isize {
        let mut grains = 0;
        while self.drop_one_sand_part_one() {
            grains += 1;
        }
        grains
    }

    fn drop_one_sand_part_one(&mut self) -> bool {
        let mut spot = Coordinate::new(500, 0);
        loop { 
            // Too far down
            if spot.y > self.bottom_right.y {
                return false;
            }

            // try going down
            let mut next = Coordinate::new(spot.x, spot.y+1);
            if let &Spot::Air = self.spots.get(&next).unwrap_or(&Spot::Air) {
                spot = next;
                continue
            }

            // down and to the left
            next.x -= 1;
            if let &Spot::Air = self.spots.get(&next).unwrap_or(&Spot::Air) {
                spot = next;
                continue
            }

            // down and to the right
            next.x += 2;
            if let &Spot::Air = self.spots.get(&next).unwrap_or(&Spot::Air) {
                spot = next;
                continue
            }

            self.spots.insert(spot, Spot::Sand);
            //println!("{}", self);
            return true
        }
    }

    fn drop_sand_part_two(&mut self) -> isize {
        let mut grains = 0;
        while self.get(&Coordinate::new(500, 0)).is_none() {
            self.drop_one_sand_part_two();
            grains += 1;
        }
        grains
    }

    fn drop_one_sand_part_two(&mut self) {
        let mut spot = Coordinate::new(500, 0);
        loop { 
            // try going down
            let mut next = Coordinate::new(spot.x, spot.y+1);
            if let &Spot::Air = self.get(&next).unwrap_or(&Spot::Air) {
                spot = next;
                continue
            }

            // down and to the left
            next.x -= 1;
            if let &Spot::Air = self.get(&next).unwrap_or(&Spot::Air) {
                spot = next;
                continue
            }

            // down and to the right
            next.x += 2;
            if let &Spot::Air = self.get(&next).unwrap_or(&Spot::Air) {
                spot = next;
                continue
            }

            self.insert(spot, Spot::Sand);
            //println!("{}", self);
            return
        }
    }
}
