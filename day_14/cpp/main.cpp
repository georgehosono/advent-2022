#include <iostream>
#include <streambuf>
#include <fstream>
#include <string>

void parse_input(std::string &s) {
	std::cout << s;
}

int main() {
	std::ifstream fs("../test", std::ios::in);
	std::string s( (std::istreambuf_iterator<char>(fs)),
			(std::istreambuf_iterator<char>()  ));
	parse_input(s);
	return 0;
}
