#! /bin/python

import re

command = re.compile(r"\$ (cd|ls) ?(.*)")
dirent = re.compile(r"dir (.+)")
file_re = re.compile(r"(\d+) (.+)")

dirs = {}
curr_path = []
def get_dir(path, dirs):
    if len(path) == 0:
        return dirs
    else:
        return get_dir(path[1:], dirs[path[0]])


with open("input", "r") as f:
    for line in f:
        cmd = command.match(line)
        curr_dir = get_dir(curr_path, dirs)
        if cmd:
            if cmd[1] == "ls":
                continue
            else:
                if cmd[2] == "..":
                    curr_path.pop(-1)
                else:
                    if cmd[2] not in curr_dir.keys():
                        curr_dir[cmd[2]] = {}
                    curr_path.append(cmd[2])
            continue

        file = file_re.match(line)
        if file:
            curr_dir[file[2]] = int(file[1])


under_100k = 0
def get_dir_size(dirent, size):
    global under_100k
    print(dirent)
    if isinstance(dirent, dict):
        for ent in dirent.values():
            size += get_dir_size(ent, 0)
        if size <= 100000:
            under_100k += size
        print(f"got size of {size}")
        return size
    else:
        return dirent

total_size = get_dir_size(dirs, 0)
print(under_100k)

free = 70000000 - total_size
min_to_delete = free
def get_dir_size(dirent, size):
    global min_to_delete
    if isinstance(dirent, dict):
        for ent in dirent.values():
            size += get_dir_size(ent, 0)
        if free + size > 30000000 and size < min_to_delete:
            min_to_delete = size
        return size
    else:
        return dirent

get_dir_size(dirs, 0)
print(min_to_delete)
