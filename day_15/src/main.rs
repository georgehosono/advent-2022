use dashmap::DashMap;
use indicatif::{ProgressBar, ProgressStyle};
use rayon::prelude::*;
use regex::Regex;
use std::{fmt, fs};

static MAX: isize = 4_000_000;
//static MAX: isize = 20;

fn main() {
    let s = fs::read_to_string("input").unwrap();
    part_one(s.as_str());
    part_two_fast(s.as_str());
}

#[allow(dead_code)]
fn part_one(s: &str) {
    let cave: Cave = s.into();
    let row = 2000000;
    //let row = 10;
    let num = (cave.top_left.x..=cave.bottom_right.x)
        .into_par_iter()
        .filter(|x| cave.test_exclusion(&Coordinate::new(*x, row)))
        .count();
    println!("part one: {}", num);
}

#[allow(dead_code)]
fn part_two_slow(s: &str) {
    let max = 400_000;
    let mut cave: Cave = s.into();
    cave.top_left = Coordinate::new(0, 0);
    cave.bottom_right = Coordinate::new(max, max);

    let bar = ProgressBar::new((max * max) as u64);
    bar.set_style(
        ProgressStyle::with_template("[{elapsed}] {human_pos:>9}/{human_len:9} ({eta})").unwrap(),
    );

    let spot = (0..=max)
        .into_par_iter()
        .flat_map(move |y| {
            (0..=max)
                .into_par_iter()
                .map(move |x| Coordinate::new(x, y))
        })
        .find_first(|coord| {
            bar.inc(1);
            !cave.test_exclusion(coord)
        });

    if let Some(spot) = spot {
        dbg!(spot);
        let tuning = 4_000_000 * spot.x + spot.y;
        println!("part two: {}", tuning);
    }
}

fn part_two_fast(s: &str) {
    let cave: Cave = s.into();

    let beacon = cave
        .spots
        .par_iter()
        .map(|r| (*r.key(), *r.value()))
        .filter_map(|(coord, spot)| {
            if let Spot::Sensor(sensor) = spot {
                Some((coord, sensor))
            } else {
                None
            }
        })
        .flat_map(|(coord, sensor)| coord.circle(coord.distance(&sensor.nearest_beacon) + 1))
        .filter(|coord| !cave.test_exclusion(coord))
        .for_each(|beacon| {
            let tuning = 400_000 * beacon.x + beacon.y;
            println!("part two: {} from {:?}", tuning, beacon);
        });

    //let tuning = 400_000 * beacon.x + beacon.y;
    //dbg!(beacon);
    //println!("part two: {}", tuning);
}

#[derive(Copy, Clone, Debug)]
struct Sensor {
    nearest_beacon: Coordinate,
}

impl Sensor {
    fn new(nearest_beacon: Coordinate) -> Self {
        Sensor { nearest_beacon }
    }
}

#[derive(Copy, Clone, Debug)]
enum Spot {
    Sensor(Sensor),
    Beacon,
    Excluded,
    Possible,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
struct Coordinate {
    x: isize,
    y: isize,
}

unsafe impl Send for Coordinate {}
unsafe impl Sync for Coordinate {}

impl Coordinate {
    fn new(x: isize, y: isize) -> Self {
        Coordinate { x, y }
    }

    fn distance(&self, other: &Coordinate) -> isize {
        (self.x.abs_diff(other.x) + self.y.abs_diff(other.y)) as isize
    }

    fn circle(self, radius: isize) -> impl ParallelIterator<Item = Coordinate> {
        assert!(radius >= 0);
        (-radius..=radius)
            .into_par_iter()
            .flat_map(move |x| {
                [-1, 1]
                    .into_par_iter()
                    .map(move |s| Coordinate::new(self.x + x, self.y + s * (radius - x.abs())))
            })
            .filter(move |coord| 0 <= coord.x && coord.x <= MAX && 0 <= coord.y && coord.y <= MAX)
    }
}

struct Cave {
    spots: DashMap<Coordinate, Spot>,
    top_left: Coordinate,
    bottom_right: Coordinate,
}

impl Cave {
    fn extend(&mut self, coord: Coordinate) {
        if coord.x < self.top_left.x {
            self.top_left.x = coord.x;
        }
        if coord.y < self.top_left.y {
            self.top_left.y = coord.y;
        }
        if coord.x > self.bottom_right.x {
            self.bottom_right.x = coord.x;
        }
        if coord.y > self.bottom_right.y {
            self.bottom_right.y = coord.y;
        }
    }

    fn insert(&mut self, coord: Coordinate, spot: Spot) -> Option<Spot> {
        self.extend(coord);
        self.spots.insert(coord, spot)
    }

    fn get(&self, coord: &Coordinate) -> Spot {
        match self.spots.get(coord) {
            Some(r) => *r.value(),
            None => Spot::Possible,
        }
    }

    fn test_exclusion(&self, coord: &Coordinate) -> bool {
        if let Some(_spot) = self.spots.get(coord) {
            // for part one
            return !matches!(*_spot, Spot::Beacon);
            //return true;
        }

        !self
            .spots
            .iter()
            .filter_map(|r| {
                let (coords, spot) = (r.key(), r.value());
                if let Spot::Sensor(sensor) = spot {
                    Some((*coords, coords.distance(&sensor.nearest_beacon)))
                } else {
                    None
                }
            })
            .find(|(sensor, dist)| coord.distance(&sensor) <= *dist)
            .is_none()
    }

    #[allow(dead_code)]
    fn fill_exclusions(&mut self) {
        let v: Vec<(Coordinate, isize)> = self
            .spots
            .iter()
            .filter_map(|r| {
                let (coords, spot) = (r.key(), r.value());
                if let Spot::Sensor(sensor) = spot {
                    Some((*coords, coords.distance(&sensor.nearest_beacon)))
                } else {
                    None
                }
            })
            .collect();

        for (coords, dist) in v.iter() {
            let dist = *dist;
            for x in -dist..=dist {
                let y_dist = dist - x.abs();
                for y in -y_dist..=y_dist {
                    let new_coords = Coordinate::new(coords.x + x, coords.y + y);
                    if self.spots.get(&new_coords).is_none() {
                        self.insert(new_coords, Spot::Excluded);
                    }
                }
            }
        }
    }
}

impl From<&str> for Cave {
    fn from(s: &str) -> Self {
        let re = Regex::new(
            r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)",
        )
        .unwrap();

        let mut cave = Cave {
            spots: DashMap::default(),
            top_left: Coordinate::new(isize::MAX, isize::MAX),
            bottom_right: Coordinate::new(isize::MIN, isize::MIN),
        };

        let it = s
            .split('\n')
            .filter(|l| !l.is_empty())
            .map(|l| re.captures(l).unwrap());
        for caps in it {
            let sensor_x: isize = caps.get(1).unwrap().as_str().parse().unwrap();
            let sensor_y: isize = caps.get(2).unwrap().as_str().parse().unwrap();
            let beacon_x: isize = caps.get(3).unwrap().as_str().parse().unwrap();
            let beacon_y: isize = caps.get(4).unwrap().as_str().parse().unwrap();

            let sensor_coords = Coordinate::new(sensor_x, sensor_y);
            let beacon_coords = Coordinate::new(beacon_x, beacon_y);
            let sensor = Sensor::new(beacon_coords);

            cave.insert(sensor_coords, Spot::Sensor(sensor));
            cave.insert(beacon_coords, Spot::Beacon);

            let dist = sensor_coords.distance(&beacon_coords);
            cave.extend(Coordinate::new(sensor_coords.x + dist, sensor_coords.y));
            cave.extend(Coordinate::new(sensor_coords.x - dist, sensor_coords.y));
            cave.extend(Coordinate::new(sensor_coords.x, sensor_coords.y + dist));
            cave.extend(Coordinate::new(sensor_coords.x, sensor_coords.y - dist));
        }

        cave
    }
}

impl fmt::Display for Cave {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        for y in self.top_left.y..=self.bottom_right.y {
            write!(f, "{:03} ", y)?;
            for x in self.top_left.x..=self.bottom_right.x {
                let coord = Coordinate::new(x, y);
                let ch = match self.get(&coord) {
                    Spot::Sensor(_) => 'S',
                    Spot::Beacon => 'B',
                    Spot::Excluded => '#',
                    Spot::Possible => '.',
                };
                write!(f, "{}", ch)?
            }
            writeln!(f)?
        }
        Ok(())
    }
}
