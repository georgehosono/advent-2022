#! /bin/python
import re
from math import lcm

class Monkey:
    def __init__(self, num, items, operation, test_num, true_dest, false_dest):
        self.num = num
        self.items = items
        self.operation = operation
        self.test_num = test_num
        self.true_dest = true_dest
        self.false_dest = false_dest
        self.inspected = 0
        self.lcm = 0

    def receive(self, n):
        self.items.append(n)

    def throw_one(self, monkeys, n):
        # print(f"inspecting item with level {n}")
        self.inspected += 1
        n = self.operation(n)
        n %= self.lcm
        # print(f"new level: {n}")
        # n //= 3
        # print(f"new level: {n}")
        if n % self.test_num == 0:
            # print(f"test passed. throwing to monkey {self.true_dest}")
            monkeys[self.true_dest].receive(n)
        else:
            # print(f"test failed. throwing to monkey {self.false_dest}")
            monkeys[self.false_dest].receive(n)

    def throw_all(self, monkeys):
        # print(f"Monkey {self.num} throwing")
        while len(self.items) > 0:
            n = self.items.pop(0)
            self.throw_one(monkeys, n)
        # print("")

    def print_items(self):
        print(f"Monkey {self.num}: {self.items}" )

    def __str__(self):
        return f"Monkey {self.num}:\n  Inspected: {self.inspected}\,  Items: {self.items}\n  Operation: {self.operation}\n  Test: divisible by {self.test_num}\n    If true: throw to monkey {self.true_dest}\n    If false: throw to monkey {self.false_dest}"


def load_file(fname):
    with open(fname, "r") as f:
        text = f.read()

    monkeys_text = text.split("\n\n")
    monkeys = {}
    reg = re.compile(r"Monkey (\d):\n  Starting items: ((?:\d+[, ]*)+)\n  Operation: new = old ([+*]) (\d+|old)\n  Test: divisible by (\d+)\n    If true: throw to monkey (\d)\n    If false: throw to monkey (\d)")
    tests = []
    for monkey in monkeys_text:
        m = reg.match(monkey)
        num = int(m[1])
        items = [int(x) for x in m[2].split(", ")]

        op = m[3]
        arg = m[4]
        if op == "*" and arg != "old":
            f = lambda old, n=int(arg): old * n
        elif op == "*":
            f = lambda old: old * old
        elif op == "+" and arg != "old":
            f = lambda old, n=int(arg): old + n
        elif op == "+":
            f = lambda old: old + old

        test = int(m[5])
        tests.append(test)
        true_dest = int(m[6])
        false_dest = int(m[7])

        monkeys[num] = Monkey(num, items, f, test, true_dest, false_dest)


    monkey_lcm = lcm(*tests)
    for num in monkeys.keys():
        monkeys[num].lcm = monkey_lcm
    return monkeys

if __name__ == "__main__":
    monkeys = load_file("input")
    for num in monkeys:
        print(str(monkeys[num]))

    for i in range(10000):
        if i % 1000 == 0 or i == 1 or i == 20:
            print(f"After round {i}:")
            for num in sorted(monkeys.keys()):
                print(f"Monkey {num} inspected items {monkeys[num].inspected} times")
            print("")
        for num in sorted(monkeys.keys()):
            monkeys[num].throw_all(monkeys)

    for num in sorted(monkeys.keys()):
        print(f"Monkey {num} inspected items {monkeys[num].inspected} times")

    inspectors = sorted([m[1].inspected for m in monkeys.items()])
    print(inspectors[-1] * inspectors[-2])
