def monkey_in_the_middle(input):
  # Parse the input and initialize the list of monkeys
  monkeys = []
  for i in range(4):
    starting_items, operation, test_true, test_false = input[i*4:i*4+4]
    monkeys.append({
      'items': list(map(int, starting_items.split(', '))),
      'operation': operation,
      'test_true': test_true,
      'test_false': test_false
    })

  # Simulate the rounds of inspection and throwing until all monkeys are holding no items
  while any(monkey['items'] for monkey in monkeys):
    for i, monkey in enumerate(monkeys):
      if not monkey['items']:
        continue
      new_items = []
      for item in monkey['items']:
        # Apply the operation and test to the item
        if '*' in monkey['operation']:
          item = item * int(monkey['operation'].split()[-1])
        elif '+' in monkey['operation']:
          item = item + int(monkey['operation'].split()[-1])
        if item % int(monkey['test_true'].split()[-1]) == 0:
          monkeys[int(monkey['test_true'].split()[-2])]['items'].append(item // 3)
        else:
          monkeys[int(monkey['test_false'].split()[-2])]['items'].append(item // 3)

  # Return the list of final items held by each monkey
  return [monkey['items'] for monkey in monkeys]

input = [
  '79, 98',
  'new = old * 19',
  'If true: throw to monkey 2',
  'If false: throw to monkey 3',
  '54, 65, 75, 74',
  'new = old + 6',
  'If true: throw to monkey 2',
  'If false: throw to monkey 0',
  '79, 60, 97',
  'new = old * old',
  'If true: throw to monkey 1',
  'If false: throw to monkey 3',
  '74',
  'new = old + 3',
  'If true: throw to monkey 0',
  'If false: throw to monkey 1',
]

monkey_in_the_middle(input)

