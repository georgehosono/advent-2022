#!/bin/python

from itertools import zip_longest
from functools import cmp_to_key

with open("input", "r") as f:
    text = f.read()
    groups = [[eval(line) for line in group.split("\n") if line] for group in text.split("\n\n") if group]

def compare(left, right):
    # print(f"compare:\n  left: {left}\n  right: {right}")
    if not isinstance(left, list):
        left = [left]
    if not isinstance(right, list):
        right = [right]
    for l, r in zip_longest(left, right, fillvalue=None):
        # print(f"zip comparing {l} and {r}")
        if l == None:
            return -1
        if r == None:
            return 1
        if isinstance(l, int) and isinstance(r, int):
            if l < r:
                # print(f"RET_A returning -1")
                return -1
            elif l > r:
                # print(f"RET_B returning 1")
                return 1
        elif isinstance(l, list) and isinstance(r, list):
            val = compare(l, r)
            if val != 0:
                # print(f"RET_C returning {val}")
                return val
        elif isinstance(l, int) and isinstance(r, list):
            val = compare([l], r)
            if val != 0:
                # print(f"RET_D returning {val}")
                return val
        elif isinstance(l, list) and isinstance(r, int):
            val = compare(l, [r])
            if val != 0:
                # print(f"RET_E returning {val}")
                return val

    # print(f"RET_G returning 0")
    return 0

count = 0
for i in range(len(groups)):
    val = compare(groups[i][0], groups[i][1])
    if val != 1:
        # print(f"group {i+1} is ok\n")
        count += i+1
print(f"part one: {count}")

flat_groups = []
for group in groups:
    flat_groups.extend(group)
flat_groups.append([[2]])
flat_groups.append([[6]])

flat_groups.sort(key=cmp_to_key(compare))
idx1 = flat_groups.index([[2]]) + 1
idx2 = flat_groups.index([[6]]) + 1
print(f"part two: {idx1 * idx2}")
